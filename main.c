/*************************************************************************
	> File Name: main.c
	> Author: QiyangXia
	> Mail: 834061456@qq.com 
	> Created Time: 2022年04月13日 星期三 14时30分50秒
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h> // for open
#include <unistd.h> // for close

#define _COLOR_RED      0x00ff0000
#define _COLOR_GREEN    0x0000ff00
#define _COLOR_BLUE     0x000000ff


long screen_size=0;
void *bg =NULL;

int main(int argc,char *argv[])
{
    int fd = -1;
	int x,y;
	int ret = 0;
	struct fb_fix_screeninfo fixinfo ={{0}};
	struct fb_var_screeninfo varinfo ={0};

	if(argc < 2)
	{
		printf("The argument is too few\n");
		return 1;
	}

    fd =open(argv[1],O_RDWR);
    if(fd < 0)
    {
		printf("open %s failed\n",argv[1]);
        return 2;
    }
    ret = ioctl(fd,FBIOGET_FSCREENINFO,&fixinfo);
    ret += ioctl(fd,FBIOGET_VSCREENINFO,&varinfo);
	if(ret < 0)
	{
		close(fd);
		fd = -1;
		printf("ioctl failed\n");
		return 2;
	}


    screen_size = varinfo.xres*varinfo.yres*(varinfo.bits_per_pixel/8);
    bg =mmap(0,screen_size,PROT_READ |PROT_WRITE,MAP_SHARED,fd,0);
    if(bg == NULL){
        printf("mmap framebuffer fail.\n");
        return -1;
    }
	printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
    printf("bits_per_pixel = %d\n",varinfo.bits_per_pixel);
    printf("Width of LCD = %d\n",varinfo.xres);
    printf("Height of LCD = %d\n",varinfo.yres);

    printf("Length of Red = %d\n",varinfo.red.length);
    printf("Length of Green = %d\n",varinfo.green.length);
    printf("Length of Blue = %d\n",varinfo.blue.length);
    printf("Offset of Red = %d\n",varinfo.red.offset);
    printf("Offset of Green = %d\n",varinfo.green.offset);
    printf("Offset of Blue = %d\n",varinfo.blue.offset);

    printf("Length of FrameBuffer Memory is %d\n",fixinfo.smem_len);
    printf("Length of Line FrameBuffer Memory is %d\n",fixinfo.line_length);

    printf("Mmap Address is %p\n",bg);
    printf("FbVInfo.xoffset = %d\n",varinfo.xoffset);
    printf("FbVInfo.yoffset = %d\n",varinfo.yoffset);
    printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");



	int *puifb = (int *)bg;

    for(y=0;y< varinfo.yres;y++){
        for(x=0;x< varinfo.xres;x++){
			*(puifb +y*varinfo.xres + x) = _COLOR_RED;
        }
    }

    munmap(bg,screen_size);
    close(fd);
    return 0;
}
