/*************************************************************************
	> File Name: ephoto.c
	> Author: QiyangXia
	> Mail: 834061456@qq.com 
	> Created Time: 2022年04月13日 星期三 13时35分26秒
 ************************************************************************/
StLCD * InitLCD(char *pstrFBDevFile)
{
	StLCD *pstLCD = NULL;
	struct fb_var_screeninfo stFbVarInfo = {0};
	struct fb_fix_screeninfo stFbFixInfo = {{0}};
	int ret = 0;
	int fd = 0;

	if(NULL == pstrFBDevFile)
	{
		PrintError0("InitLCD Input Param is invalid\n");
		return NULL;
	}

	fd = open(pstrFBDevFile,O_RDWR);
	if(fd < 0)
	{
		PrintError1("InitLCD Open %s Failed\n",pstrFBDevFile);
		return NULL;
	}

	ret = xioctl(fd,FBIOGET_VSCREENINFO,&stFbVarInfo);
	ret = xioctl(fd,FBIOGET_FSCREENINFO,&stFbFixInfo);
	if(ret < 0)
	{
		PrintError0("InitLCD ioctl for framebuff failed\n");
		close(fd);
		return NULL;
	}
	if(stFbVarInfo.bits_per_pixel / 8 <= 1)
	{
		PrintError0("InitLCD This LCD is not supported\n");
		close(fd);
		return NULL;
	}
	pstLCD = (StLCD *)malloc(sizeof(StLCD));
	if(NULL == pstLCD)
	{
		PrintError0("InitLCD Malloc Failed\n");
		close(fd);
		return NULL;
	}
	memset(pstLCD,0x00,sizeof(StLCD));

	pstLCD->fd = fd;
	pstLCD->ulBPP = stFbVarInfo.bits_per_pixel;
	pstLCD->ulLCDW = stFbVarInfo.xres;
	pstLCD->ulLCDH = stFbVarInfo.yres;
	pstLCD->ulRLen = stFbVarInfo.red.length;
	pstLCD->ulGLen = stFbVarInfo.green.length;
	pstLCD->ulBLen = stFbVarInfo.blue.length;
	pstLCD->ulROffset = stFbVarInfo.red.offset;
	pstLCD->ulGOffset = stFbVarInfo.green.offset;
	pstLCD->ulBOffset = stFbVarInfo.blue.offset;
	pstLCD->ulLenOfFBMemory = stFbFixInfo.smem_len;
	pstLCD->ulLenOfLine = stFbFixInfo.line_length;

	pstLCD->pvFBMem = mmap(0,pstLCD->ulLenOfFBMemory,PROT_READ|PROT_WRITE,MAP_SHARED,pstLCD->fd,0);
	if(pstLCD->pvFBMem == MAP_FAILED)
	{
		PrintError0("InitLCD mmap for framebuffer failed\n");
		close(pstLCD->fd);
		free(pstLCD);
		return NULL;
	}

	PrintInfo0("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	PrintInfo1("bits_per_pixel = %d\n",(int)pstLCD->ulBPP);
	PrintInfo1("Width of LCD =%d\n",(int)pstLCD->ulLCDW);
	PrintInfo1("Height of LCD =%d\n",(int)pstLCD->ulLCDH);

	PrintInfo1("Length of Red = %d\n",(int)pstLCD->ulRLen);
	PrintInfo1("Length of Green = %d\n",(int)pstLCD->ulGLen);
	PrintInfo1("Length of Blue = %d\n",(int)pstLCD->ulBLen);
	PrintInfo1("Offset of Red = %d\n",(int)pstLCD->ulROffset);
	PrintInfo1("Offset of Green = %d\n",(int)pstLCD->ulGOffset);
	PrintInfo1("Offset of Blue = %d\n",(int)pstLCD->ulBOffset);

	PrintInfo1("Length of FrameBuffer Memory is %d\n",(int)pstLCD->ulLenOfFBMemory);
	PrintInfo1("Lenght of Line FrameBuffer Memory is %d\n",(int)pstLCD->ulLenOfLine);

	PrintInfo1("Mmap Addresss is %p\n",pstLCD->pvFBMem);
	PrintInfo1("FbVInof.xoffset = %d\n",(int)stFbVarInfo.xoffset);
	PrintInfo1("FbVInof.yoffset = %d\n",(int)stFbVarInfo,yoffset);
	PrintInfo0("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	
	return pstLCD;
}



